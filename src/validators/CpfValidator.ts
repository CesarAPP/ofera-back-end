import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

@ValidatorConstraint({ name: 'CpfValidation', async: false })
export class CpfValidator implements ValidatorConstraintInterface {
    validate(text: string) {
        if (!text) return;
        let Soma = 0;
        let Resto = 0;
        if (text == "00000000000") return false;

        for (let i = 1; i <= 9; i++) Soma = Soma + parseInt(text.substring(i - 1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11)) Resto = 0;
        if (Resto != parseInt(text.substring(9, 10))) return false;

        Soma = 0;
        for (let i = 1; i <= 10; i++) Soma = Soma + parseInt(text.substring(i - 1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11)) Resto = 0;
        if (Resto != parseInt(text.substring(10, 11))) return false;
        return true;
    }

    defaultMessage() {
        return 'Este CPF não é válido, por favor verifique seus dados e tente novamente!';
    }
}