export enum AuthenticationErrors {
  'auth/invalid-email' = 'Digite um e-mail válido para continuar',
  'auth/user-not-found' = 'Usuário não encontrado, por favor verifique os dados e tente novamente',
  'auth/wrong-password' = 'Senha incorreta, por favor verifique os dados e tente novamente',
  'auth/email-already-exists' = 'Este e-mail já está cadastrado, por favor verifique os dados e tente novamente',
  'auth/weak-password' = 'Senha fraca, sua senha precisa conter ao menos seis dígitos',
  'auth/user-disabled' = 'Sua conta está desabilitada, por favor entre em contato com o SAC',
}
