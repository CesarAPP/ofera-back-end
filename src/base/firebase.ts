import { BadRequestException } from '@nestjs/common';
import { firestore, auth } from 'firebase-admin';
import { AuthenticationErrors } from '../base/errors.enum';
export class FirebaseConnection {

  protected db = firestore();
  protected dbRef = firestore().collection(this.collection);

  constructor(private collection: string) { }

  protected async createAuthentication(payload) {
    let uid: string = null;
    try {
      uid = (
        await auth().createUser({
          email: payload.email,
          password: payload.password,
          displayName: payload.name,
        })
      ).uid;
      delete payload.password;
      payload = {
        id: uid,
        createdAt: new Date(),
        ...payload,
      };
      await this.insertWithId(uid, payload);
      return payload;
    } catch (err) {
      const { code } = err;
      if (uid) {
        await auth().deleteUser(uid);
        await this.delete(uid);
      }
      throw new BadRequestException(
        AuthenticationErrors[code] ||
        'Ocorreu um erro ao criar sua conta, por favor tente novamente',
      );
    }
  }

  protected getById(id: string) {
    return this.dbRef.doc(id).get();
  }

  protected getAll() {
    return this.dbRef.get();
  }

  protected insert(data: any) {
    return this.dbRef.add({ ...data });
  }

  protected insertWithId(id: string, data: any) {
    return this.dbRef.doc(id).set({ ...data, id });
  }

  protected delete(id: string) {
    return this.dbRef.doc(id).delete();
  }

  protected update(id: string, data: any) {
    return this.dbRef.doc(id).update({ ...data });
  }

  protected getId() {
    return Date.now().toString() + Math.floor(100000 + Math.random() * 900000).toString();
  }
}
