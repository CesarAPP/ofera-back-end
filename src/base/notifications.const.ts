export const Notifications = {
    "Cozinha": {
        title: 'Já estamos preparando!',
        body: 'Seu pedido já está sendo preparado, e logo será enviado!',
    },
    "Em entrega": {
        title: 'Estamos a caminho!',
        body: 'Seu pedido saiu para entrega!',
    },
    "Entregue": {
        title: 'Pedido entregue!',
        body: 'Seu pedido foi entregue! Obrigado pela preferência!',
    },
    "Cancelado": {
        title: 'Poxa, tivemos um problema.',
        body: 'Seu pedido foi cancelado.',
    },
    "Recusado": {
        title: 'Poxa, tivemos um problema.',
        body: 'Não conseguiremos preparar o seu pedido.',
    }
}