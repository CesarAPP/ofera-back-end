import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { initializeFirebase } from './config/firebase.config';

import * as fs from 'fs';
import * as dotenv from 'dotenv';
import { BadRequestException, ValidationError, ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  dotenv.config();
  initializeFirebase();

  const httpsOptions = {
    key: fs.readFileSync('./keys/privadakey33357.key'),
    cert: fs.readFileSync('./keys/cert.pem'),
  };

  const app = await NestFactory.create(AppModule, {
    // httpsOptions,
    cors: true,
    snapshot: true
  });

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      exceptionFactory: (validationErrors: ValidationError[] = []) => {
        let deepness = 1;
        const getMessages = (e: ValidationError) => {
          let validationMessages = [];
          for (const property in e.constraints) {
            validationMessages.push(e.constraints[property]);
          }
          if (e.children.length > 0) {
            deepness++;
            validationMessages = [
              ...validationMessages,
              ...e.children.map((c) => getMessages(c)),
            ];
          }
          return validationMessages;
        };
        return new BadRequestException(
          validationErrors.map(getMessages).flat(deepness),
        );
      },
    }),
  );

  await app.listen(process.env.PORT || 7676);
}
bootstrap();
