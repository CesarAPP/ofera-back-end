import { Body, Controller, Get, Param, Patch, Post, Query, Req } from '@nestjs/common';
import { Request } from 'express';
import { OrderService } from './order.service';
import { CreateOrderDTO } from './dto/create-order.dto';

@Controller('order')
export class OrderController {
  constructor(private readonly orderService: OrderService) { }

  @Get('getDistanceFromStoreToAddress')
  getDistanceFromStoreToAddress(@Query('to') to: string, @Query('from') from: string) {
    return this.orderService.getDistanceFromStoreToAddress(from, to);
  }

  @Post('web')
  createWeb(@Body() payload: CreateOrderDTO, @Req() request: Request) {
    payload.ip = request.ip;
    return this.orderService.create(payload);
  }

  @Patch('/changeStatus/:id')
  changeStatusOrder(@Param('id') id: string, @Body() payload: any) {
    return this.orderService.changeStatusOrder(id, payload);
  }
}
