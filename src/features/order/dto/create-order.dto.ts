import { Type } from "class-transformer";
import { Allow, IsBoolean, IsNotEmpty, IsNumber, IsString, Length, MaxLength, MinLength, Validate, ValidateIf, ValidateNested } from "class-validator";
import { CpfValidator } from "src/validators/CpfValidator";

export class Store {

    @Allow()
    badge: string;


    @IsNotEmpty({ message: 'Ocorreu um erro restaurante, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro restaurante, por favor tente novamente' })
    groupId: string;


    @IsNotEmpty({ message: 'Ocorreu um erro restaurante, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro restaurante, por favor tente novamente' })
    id: string;


    @Allow()
    lat: number;


    @Allow()
    lng: number;


    @Allow()
    marca: string;


    @Allow()
    insignia: string;


    @Allow()
    unit: string;


    @Allow()
    otherIDs: any;

    @Allow()
    address: any;
}

export class Promo {

    @Allow()
    code: string;


    @Allow()
    customCode: string;


    @Allow()
    description: string;


    @Allow()
    from: string;


    @Allow()
    id: string;


    @Allow()
    target: string;


    @Allow()
    value: number;
}

export class Option {


    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar adicional do produto, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro ao detectar adicional do produto, por favor tente novamente' })
    name: string;


    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar adicional do produto, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro ao detectar adicional do produto, por favor tente novamente' })
    optionId: string;


    @IsNotEmpty({ message: 'Ocorreu um erro ao calcular o valor produto, por favor tente novamente' })
    @IsNumber({ allowInfinity: false, allowNaN: false }, { message: 'Ocorreu um erro ao calcular o valor produto, por favor tente novamente' })
    price: number;


    @IsNotEmpty({ message: 'Ocorreu um erro ao calcular o valor produto, por favor tente novamente' })
    @IsNumber({ allowInfinity: false, allowNaN: false }, { message: 'Ocorreu um erro ao calcular o valor produto, por favor tente novamente' })
    quantity: number;


    @IsNotEmpty({ message: 'Ocorreu um erro ao calcular o valor produto, por favor tente novamente' })
    @IsNumber({ allowInfinity: false, allowNaN: false }, { message: 'Ocorreu um erro ao calcular o valor produto, por favor tente novamente' })
    unitPrice: number;


    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar variação do produto, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro ao detectar variação do produto, por favor tente novamente' })
    variationId: string;


    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar variação do produto, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro ao detectar variação do produto, por favor tente novamente' })
    variationName: string;
}

export class Product {


    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar o produto, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro ao detectar o produto, por favor tente novamente' })
    id: string;


    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar o produto, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro ao detectar o produto, por favor tente novamente' })
    name: string;


    @ValidateIf((v) => v.obs)
    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar o produto, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro ao detectar o produto, por favor tente novamente' })
    obs: string;


    @ValidateNested({ each: true })
    @ValidateIf((v) => v?.options?.length > 0)
    @IsNotEmpty({ message: 'Nenhuma opção selecionada.' })
    @Type(() => Option)
    options: Option[];


    @IsNotEmpty({ message: 'Ocorreu um erro ao calcular o valor do produto, por favor tente novamente' })
    @IsNumber({ allowInfinity: false, allowNaN: false }, { message: 'Ocorreu um erro ao calcular o valor do produto, por favor tente novamente' })
    optionsPrice: number;


    @IsNotEmpty({ message: 'Ocorreu um erro ao calcular o valor do produto, por favor tente novamente' })
    @IsNumber({ allowInfinity: false, allowNaN: false }, { message: 'Ocorreu um erro ao calcular o valor do produto, por favor tente novamente' })
    quantity: number;


    @IsNotEmpty({ message: 'Ocorreu um erro ao calcular o valor do produto, por favor tente novamente' })
    @IsNumber({ allowInfinity: false, allowNaN: false }, { message: 'Ocorreu um erro ao calcular o valor do produto, por favor tente novamente' })
    totalPrice: number;


    @IsNotEmpty({ message: 'Ocorreu um erro ao calcular o valor do produto, por favor tente novamente' })
    @IsNumber({ allowInfinity: false, allowNaN: false }, { message: 'Ocorreu um erro ao calcular o valor do produto, por favor tente novamente' })
    unitPrice: number;


    @Allow()
    metadata?: any;
}

export class Card {

    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar o método do pagamento, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro ao detectar o método do pagamento, por favor tente novamente' })
    @ValidateIf((v) => v.brand)
    brand?: string;


    @ValidateIf((v) => v.token)
    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar o método do pagamento, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro ao detectar o método do pagamento, por favor tente novamente' })
    token?: string;


    @ValidateIf((v) => v.description)
    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar o método do pagamento, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro ao detectar o método do pagamento, por favor tente novamente' })
    description?: string;


    @Allow()
    identifier?: string;
}

export class Payment {


    @ValidateNested({ each: true })
    @IsNotEmpty({ message: 'Nenhum cartão selecionado.' })
    @Type(() => Card)
    card: Card;


    @IsNotEmpty({ message: 'Ocorreu um erro ao calcular valor do troco, por favor tente novamente' })
    @IsNumber({ allowInfinity: false, allowNaN: false }, { message: 'Ocorreu um erro ao calcular valor do troco, por favor tente novamente' })
    change: number;


    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar o método do pagamento, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro ao detectar o método do pagamento, por favor tente novamente' })
    method: string;


    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar o tipo do pagamento, por favor tente novamente' })
    @IsString({ message: 'Ocorreu um erro ao detectar o tipo do pagamento, por favor tente novamente' })
    type: string;


    @IsNotEmpty({ message: 'Ocorreu um erro ao calcular valor do pedido, por favor tente novamente' })
    @IsNumber({ allowInfinity: false, allowNaN: false }, { message: 'Ocorreu um erro ao calcular valor do pedido, por favor tente novamente' })
    value: number;
}

export class Address {


    @IsString({ message: 'Preencha o bairro no seu endereço para continuar.' })
    @IsNotEmpty({ message: 'Preencha o bairro no seu endereço para continuar.' })
    bairro: string;


    @IsString({ message: 'Preencha o CEP no seu endereço para continuar.' })
    @IsNotEmpty({ message: 'Preencha o CEP no seu endereço para continuar.' })
    cep: string;


    @IsString({ message: 'Preencha a cidade no seu endereço para continuar.' })
    @IsNotEmpty({ message: 'Preencha a cidade no seu endereço para continuar.' })
    cidade: string;


    @ValidateIf((v) => v.complemento)
    @IsString({ message: 'Preencha o complemento no seu endereço para continuar.' })
    @IsNotEmpty({ message: 'Preencha o complemento no seu endereço para continuar.' })
    complemento: string;


    @IsString({ message: 'Preencha o número no seu endereço para continuar.' })
    @IsNotEmpty({ message: 'Preencha o número no seu endereço para continuar.' })
    id: string;


    @IsNotEmpty({ message: 'Preencha seu endereço completamente para continuar 2.' })
    lat: number;


    @IsNotEmpty({ message: 'Preencha seu endereço completamente para continuar. 4' })
    lng: number;


    @IsString({ message: 'Preencha o número no seu endereço para continuar.' })
    @IsNotEmpty({ message: 'Preencha o número no seu endereço para continuar.' })
    numero: string;


    @IsString({ message: 'Preencha o logradouro no seu endereço para continuar.' })
    @IsNotEmpty({ message: 'Preencha o logradouro no seu endereço para continuar.' })
    rua: string;


    @IsString({ message: 'Preencha a UF no seu endereço para continuar.' })
    @IsNotEmpty({ message: 'Preencha a UF no seu endereço para continuar.' })
    uf: string;
}

export class Delivery {


    @ValidateNested({ each: true })
    @IsNotEmpty({ message: 'Nenhum endereço selecionado.' })
    @Type(() => Address)
    address: Address;


    @ValidateIf((v) => v.recebimento)
    @IsNotEmpty({ message: 'Nenhum método de entrega selecionado.' })
    @IsString({ message: 'Nenhum método de entrega selecionado.' })
    recebimento: string;
}

export class Fiscal {

    @IsBoolean({ message: 'Deve ser informado se o cliente necessita da nota fiscal.' })
    @IsNotEmpty()
    docRequested: boolean;

    @IsString({ message: 'O documento deve ser CPF válido para solicitar nota fiscal.' })
    @IsNotEmpty({ message: 'O documento deve ser CPF válido para solicitar nota fiscal.' })
    @Validate(CpfValidator, { message: 'O documento deve ser CPF válido para solicitar nota fiscal.' })
    @ValidateIf((v) => v.document || v.docRequested)
    document?: string;
}

export class Customer {


    @IsString({ message: 'Usuário não encontrado, por favor tente novamente.' })
    @IsNotEmpty({ message: 'Usuário não encontrado, por favor tente novamente.' })
    id: string;


    @IsNotEmpty({ message: 'Nome do usuário inválido, por favor tente novamente.' })
    @IsString({ message: 'Nome do usuário inválido, por favor tente novamente.' })
    name: string;


    @ValidateIf((v) => v.email)
    @IsNotEmpty({ message: 'Email do usuário inválido, por favor tente novamente.' })
    @IsString({ message: 'Email do usuário inválido, por favor tente novamente.' })
    email?: string;


    @ValidateIf((v) => v.phone)
    @Length(10, 11, { message: 'Telefone do usuário inválido, por favor tente novamente.' })
    @IsNotEmpty({ message: 'Telefone do usuário inválido, por favor tente novamente.' })
    @IsString({ message: 'Telefone do usuário inválido, por favor tente novamente.' })
    phone?: string;


    @ValidateNested({ each: true })
    @IsNotEmpty({ message: 'Dados fiscais inválidos.' })
    @Type(() => Fiscal)
    fiscal: Fiscal;
}

export class Config {


    @IsNotEmpty({ message: 'Ocorreu um erro no cálculo do frete, por favor tente novamente.' })
    @IsNumber({ allowInfinity: false, allowNaN: false }, { message: 'Ocorreu um erro ao calcular frete, por favor tente novamente.' })
    deliveryTiming: number;


    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar tipo do frete, por favor tente novamente.' })
    @IsString({ message: 'Ocorreu um erro ao detectar tipo do frete, por favor tente novamente.' })
    deliveryType: string;


    @IsNotEmpty({ message: 'Ocorreu um erro ao detectar o tipo do pedido, por favor tente novamente.' })
    @IsString({ message: 'Ocorreu um erro ao detectar o tipo do pedido, por favor tente novamente.' })
    orderType: string;
}

export class CreateOrderDTO {


    @ValidateNested({ each: true })
    @IsNotEmpty()
    @Type(() => Config)
    config: Config;


    @ValidateNested({ each: true })
    @IsNotEmpty()
    @Type(() => Customer)
    customer: Customer;


    @ValidateNested({ each: true })
    @IsNotEmpty()
    @Type(() => Store)
    store: Store;


    @ValidateIf((obj) => obj.config.orderType === 'Delivery')
    @ValidateNested({ each: true })
    @IsNotEmpty()
    @Type(() => Delivery)
    delivery: Delivery;

    metadata: any[];


    @ValidateNested({ each: true })
    @IsNotEmpty()
    @Type(() => Payment)
    payment: Payment;


    @ValidateNested({ each: true })
    @IsNotEmpty()
    @Type(() => Product)
    products: Product[];


    @Allow()
    promo: any[];


    @ValidateIf((v) => v?.schedule)
    schedule: any;


    @IsNotEmpty({ message: 'Ocorreu um erro ao calcular o valor do frete, por favor tente novamente.' })
    @IsNumber({ allowInfinity: false, allowNaN: false }, { message: 'Ocorreu um erro ao calcular o valor do frete, por favor tente novamente.' })
    deliveryPrice: number;


    @Allow()
    platform?: string;


    @Allow()
    ip?: string;


    @Allow()
    token?: string;


    @Allow()
    version?: string;
}