import { HttpService } from '@nestjs/axios';
import { BadRequestException, Injectable } from '@nestjs/common';
import { firstValueFrom } from 'rxjs';
import { FirebaseConnection } from 'src/base/firebase';
import { CreateOrderDTO } from './dto/create-order.dto';
import { firestore } from 'firebase-admin';

@Injectable()
export class OrderService extends FirebaseConnection {

    constructor(
        private http: HttpService
    ) {
        super('products_orders')
    }

    async create(payload: CreateOrderDTO) {
        const orderId = this.getId();

        const store = (await this.db.collection('lojas_unidades').doc(payload.store.id).get()).data();

        if (!store) {
            throw new BadRequestException('Esta loja encontra-se fechada no momento.');
        }

        if (!this.checkIfStoreIsOpen(store)) {
            throw new BadRequestException('Esta loja está fechada no momento.');
        }

        if (payload.products.some(p => p.totalPrice <= 0)) {
            throw new BadRequestException('Seu pedido não pode conter itens com valor zero.');
        }

        return await this.placeOrder(payload, orderId);
    }

    async getDistanceFromStoreToAddress(from: string, to: string) {
        const res = (await firstValueFrom(this.http.get(`https://maps.googleapis.com/maps/api/distancematrix/json?destinations=${to}&origins=${from}&key=AIzaSyCRVL1uX7BUSu2ROmtxB8wfQDxTh9wdoy0`))).data;
        return res;
    }

    async placeOrder(payload, id) {
        payload = JSON.parse(JSON.stringify(payload));
        delete payload.token
        const userWallet = (await this.db.collection('user_wallets').doc(payload.customer.id).get())?.data();
        const productsValue = payload.products.reduce((acc, cur) => acc + cur.totalPrice, 0);
        const order = {
            ...payload,
            customer: {
                ...payload.customer,
                totalOrders: userWallet?.flags?.orders || 0
            },
            metadata: [],
            createdAt: new Date(),
            modifiedAt: null,
            promo: payload?.promo.map(cupom => {
                const coupon = cupom;
                delete coupon.createdAt;
                delete coupon.dateEnd;
                delete coupon.dateStart;
                delete coupon.elegibility;
                delete coupon.rules;
                delete coupon.segmentationField;
                delete coupon.segmentationType;
                delete coupon.segmentationValues;
                delete coupon.uses;
                delete coupon.visible;
                return coupon;
            }) || [],
            source: {
                app: "Local",
                platform: payload.platform,
                ip: payload.ip
            },
            id: id,
            displayId: id,
            status: 'Aguardando aprovação',
            statusLog: [],
            updatedPayment: [payload.payment],
            values: {
                delivery: payload.deliveryPrice,
                extras: 0,
                discount: 0,
                extrasDetailed: [],
                subTotal: productsValue,
                total: (productsValue + payload.deliveryPrice)
            }
        }
        delete order.ip
        delete order.deliveryPrice;
        delete order.platform;
        delete order.payment.card.token;
        try {
            (await (this.insertWithId(id, order))).writeTime;
        } catch (err) {

        }
        return { id: order.id };
    }

    async changeStatusOrder(id, payload) {
        const order = (await this.getById(id)).data();

        if (!order.id) {
            throw new BadRequestException('Pedido não encontrado');
        }

        const updateDoc: any = {
            statusLog: firestore.FieldValue.arrayUnion({
                oldStatus: order.status,
                newStatus: payload.status,
                at: new Date(),
                by: payload.userName,
                userId: payload.userId,
            }),
            status: payload.status,
            modifiedAt: new Date()
        }

        if (payload.status === 'Em entrega') {
            updateDoc.motoboy = payload.motoboy || {}
        }

        return this.update(id, updateDoc);
    }

    private checkIfStoreIsOpen(store) {

        if (!store.active) return false;

        const dateString = new Date().toLocaleString('pt-BR', { timeZone: "America/Sao_Paulo" }).replace(',', '');
        const actualHour = +dateString.split(' ')[1].split(':').slice(0, 2).join('');
        const day = new Date(dateString.split(' ')[0].split('/').reverse().join('-') + 'T12:00:00').getDay();
        const hourConfigs = store?.horarios.find(h => h.diaDaSemana == day);

        if (!hourConfigs || !hourConfigs.aberta || !store.active) {
            return false;
        }

        return hourConfigs.hours.some(hour => {
            return actualHour >= +hour.start.split(':').join('') && actualHour <= +hour.end.split(':').join('')
        });
    }
}
