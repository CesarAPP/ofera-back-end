import { Module } from '@nestjs/common';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    ConfigModule.forRoot(),
    HttpModule.register({
      timeout: 45000,
    }),
    ScheduleModule.forRoot()],
  controllers: [OrderController],
  providers: [OrderService]
})
export class OrderModule { }
