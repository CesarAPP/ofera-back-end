import { Injectable } from '@nestjs/common';
import { FirebaseConnection } from 'src/base/firebase';

@Injectable()
export class CronsService extends FirebaseConnection {

    constructor() {
        super('products_orders');
    }

    async closePastOrders() {
        const d1 = new Date();
        const d2 = new Date();
        d1.setDate(d1.getDate() - 1);
        d2.setDate(d2.getDate() - 2);

        let orders = (await this.dbRef.where('createdAt', '>=', d2).where('createdAt', '<', d1).get()).docs.map(doc => doc.data());
        orders = orders.filter(f => f.status != "Entregue" && f.status != "Cancelado" && f.status != "Recusado");

        const batches = [this.db.batch()];
        let count = 0;

        for (let i = 0; i < orders.length; i++) {
            const order = orders[i];

            if (count >= 500) {
                batches.push(this.db.batch());
                count = 0;
            }

            if (order.status == "Em entrega" || order.status == "Pronto para retirar" || order.status == "Cozinha") {
                batches[batches.length - 1].update(this.dbRef.doc(order.id), { status: "Entregue" });
                count++;
            }

            if (order.status == "Aguardando aprovação") {
                batches[batches.length - 1].update(this.dbRef.doc(order.id), { status: "Recusado" });
                count++;
            }
        }

        for (let i = 0; i < batches.length; i++) {
            const batch = batches[i];
            await batch.commit();
        }
    }

}
