import { Controller, Logger } from '@nestjs/common';
import { CronsService } from './crons.service';
import { Cron } from '@nestjs/schedule';

@Controller('crons')
export class CronsController {
  
  constructor(private readonly cronsService: CronsService) {}

  @Cron('15 * * * *', { timeZone: 'America/Sao_Paulo' })
  closePastOrders() {
    Logger.debug('Closing past orders...', 'CronsController');
    this.cronsService.closePastOrders();
  }

}
