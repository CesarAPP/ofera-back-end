import { FirebaseConnection } from "./../../base/firebase";
import { BadRequestException, Injectable } from "@nestjs/common";
import * as turf from "@turf/turf"

@Injectable()
export class StoreService extends FirebaseConnection {
  constructor() {
    super("lojas_unidades");
  }

  async getAvailableStores(lat, lng, insignia, id = '') {
    let response = null;

    const minLNG = lng - (50 / 111);
    const maxLNG = lng + (50 / 111);

    try {
      if (id) {
        response = await Promise.all([
          this.db.collection('configuracoes_franquias').get(),
          this.dbRef.where('active', '==', true).where("address.lng", ">", minLNG).where("address.lng", "<", maxLNG).where('id', '==', id).get()
        ]);
      } else {
        response = await Promise.all([
          this.db.collection('configuracoes_franquias').get(),
          this.dbRef.where('active', '==', true).where("address.lng", ">", minLNG).where("address.lng", "<", maxLNG).get()
        ]);
      }
    } catch (err) {
      throw new BadRequestException(err);
    }

    let [admBadges, storeDocs] = response;

    if (storeDocs.docs.empty || admBadges.docs.empty) {
      return [];
    }

    admBadges = admBadges.docs.map(doc => doc.data());
    
    if (!id) {
      storeDocs = storeDocs.docs.map(doc => doc.data()).filter(store => store.insignia == insignia);
    } else {
      storeDocs = storeDocs.docs.map(doc => doc.data());
    }

    let storesInRadius = [];

    for (let store of storeDocs) {
      try {
        const polyArr = turf.polygon([this.repeatLastItem(store.areaDeEntrega.map(a => [a.lat, a.lng]))]);
        const point = [lat, lng];

        if (!turf.booleanPointInPolygon(point, polyArr)) {
          continue;
        }

        if (storesInRadius.findIndex(l => l.insignia == store.insignia) === -1) {
          storesInRadius.push(store);
          continue;
        }

        const lojaInsignia = storesInRadius[storesInRadius.findIndex(l => l.insignia == store.insignia)];

        if (this.calculateDistance(lojaInsignia.address.lat, lojaInsignia.address.lng, lat, lng) > this.calculateDistance(store.address.lat, store.address.lng, lat, lng)) {
          storesInRadius[storesInRadius.findIndex(l => l.insignia == store.insignia)] = store;
        }
      } catch (err) {
        console.log(store.insignia, store.id);
        continue;
      }
    }

    storesInRadius = storesInRadius.map((loja) => {
      const { id, insignia } = loja;
      const { img, name, subtitle, login_logo_mini, login_bg } = admBadges.find(badge => badge.id == insignia);
      const distance = this.calculateDistance(loja.address.lat, loja.address.lng, lat, lng);
      const time = loja.configEntrega.find(c => (distance >= c.min && distance <= c.max)).tempo;
      const frete = loja.configEntrega.find(c => (distance >= c.min && distance <= c.max)).valor;
      const online = this.checkIfStoreIsOpen(loja);
      return { id, insignia, distance: parseFloat(distance.toFixed(1)), time, frete, login_bg, open: loja.open, img, name, subtitle, online, login_logo_mini, unidade: loja?.unidade, horarios: loja?.horarios };
    });

    console.log(storesInRadius);

    if (id) {
      return storesInRadius[0] || [];
    }

    const offline = storesInRadius.filter(store => !store.online);
    const online = storesInRadius.filter(store => store.online);

    return [...online.sort((a, b) => {
      const r1 = Math.random();
      const r2 = Math.random();
      if (r1 > r2) {
        return -1;
      } else if (r1 < r2) {
        return 1;
      }

      return 0;
    }), ...offline]
  }

  async getStore(id: string) {
    const [store, menu] = (await Promise.all([
      this.getById(id),
      this.db.collection('lojas_cardapios').doc(id).get()
    ])).map((doc) => doc.data());
    if (store?.id) {
      return { ...store, ...menu };
    } else {
      throw new BadRequestException("Loja não encontrada");
    }
  }

  async getProduct(id: string) {
    return (await this.db.collection('products_variations').doc(id).get()).data();
  }

  private checkIfStoreIsOpen(store) {
    const dateString = new Date().toLocaleString('pt-BR', { timeZone: "America/Sao_Paulo" }).replace(',', '');
    const actualHour = +dateString.split(' ')[1].split(':').slice(0, 2).join('');
    const day = new Date(dateString.split(' ')[0].split('/').reverse().join('-') + 'T12:00:00').getDay();
    const hourConfigs = store?.horarios.find(h => h.diaDaSemana == day);

    if (!hourConfigs || !hourConfigs.aberta || !store.active) {
      return false;
    }

    return hourConfigs.hours.some(hour => {
      return actualHour >= +hour.start.split(':').join('') && actualHour <= +hour.end.split(':').join('')
    });
  }

  private calculateDistance(lojaLat, lojaLng, userLat, userLng) {
    lojaLng = lojaLng * Math.PI / 180;
    userLng = userLng * Math.PI / 180;
    lojaLat = lojaLat * Math.PI / 180;
    userLat = userLat * Math.PI / 180;
    const dlon = userLng - lojaLng;
    const dlat = userLat - lojaLat;
    const a = Math.pow(Math.sin(dlat / 2), 2) + Math.cos(lojaLat) * Math.cos(userLat) * Math.pow(Math.sin(dlon / 2), 2);
    const c = 2 * Math.asin(Math.sqrt(a));
    const r = 6371;
    return (c * r);
  }

  private repeatLastItem(arr) {
    let returnArray = [];
    if (arr.length) {
      returnArray = arr;
      returnArray.push(arr[0]);
    }
    return returnArray;
  }
}
