import { Controller, Get, Param } from "@nestjs/common";
import { StoreService } from "./store.service";
import { Cron } from "@nestjs/schedule";

@Controller("store")
export class StoreController {
  constructor(private readonly storeService: StoreService) { }

  @Get("/product/:id")
  getProduct(@Param("id") id: string) {
    console.log(id);
    return this.storeService.getProduct(id);
  }

  @Get(":lat/:lng/:insignia/:id")
  getStoresById(@Param("lat") lat: number, @Param("lng") lng: number, @Param("id") id: string) {
    console.log(lat, lng, id);
    return this.storeService.getAvailableStores(lat, lng, '', id);
  }

  @Get(":lat/:lng/:insignia")
  getStores(@Param("lat") lat: number, @Param("lng") lng: number, @Param("insignia") insignia: string) {
    return this.storeService.getAvailableStores(lat, lng, insignia);
  }

  @Get(":id")
  getStore(@Param("id") id: string) {
    return this.storeService.getStore(id);
  }

}
