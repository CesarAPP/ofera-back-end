import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HttpModule } from '@nestjs/axios';
import { ScheduleModule } from '@nestjs/schedule';
import { CronsModule } from './features/crons/crons.module';
import { OrderModule } from './features/order/order.module';
import { StoreModule } from './features/store/store.module';

@Module({
  imports: [HttpModule, CronsModule, OrderModule, StoreModule, ScheduleModule.forRoot()],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
